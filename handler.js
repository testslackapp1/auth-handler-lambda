const AWS = require('aws-sdk');

const axios = require('axios');
const qs = require('query-string');

const apiUrl = 'https://slack.com/api';

const dynamoDb = new AWS.DynamoDB.DocumentClient({
  api_version: '2012-08-10',
  region: 'us-west-1'
});

exports.handler = async (request) => {
  const { queryStringParameters: { code } } = request;

  console.log(code);

  const data = {
    client_id: '517598472918.1390022067571',
    client_secret: 'e191491640add666e8c369a8e963ece5',
    redirect_uri: 'https://2ndpmwxkka.execute-api.us-west-1.amazonaws.com/auth/redirect',
    code,
  };
  const result = await axios.post(apiUrl + '/oauth.v2.access', qs.stringify(data), { headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  }});

  if (result.data.ok) {
    const params = {
      TableName: 'slackUsers',
      Item: {
        'id' : result.data.authed_user.id,
        'authToken' : result.data.access_token,
      }
    };

    await dynamoDb.put(params).promise();
  }

  return { statusCode: 200, body: JSON.stringify('Thank you!') };
};
